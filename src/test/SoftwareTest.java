package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Student;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 600);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Student stu1 = new Student(1587,5000);
		Student stu2 = new Student(1589,3000);
		
		stu1.setName("SomChai");
		frame.setResult(stu1.getBalance());
		frame.extendResult(stu1.dePosit(500));
		frame.extendResult(stu1.withDraw(100));
		frame.extendResult(stu1.withDraw(500));
		frame.extendResult(stu1.withDraw(1500));
		
		frame.extendResult("-----------------------------------------------------------\n");
		stu2.setName("Chulee");
		frame.extendResult(stu2.getBalance());
		frame.extendResult(stu2.withDraw(100));
		frame.extendResult(stu2.withDraw(500));
		frame.extendResult(stu2.dePosit(800));
		
		
		


	}

	ActionListener list;
	SoftwareFrame frame;
}
